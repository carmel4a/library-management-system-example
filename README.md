# Library management system example

This project is an example implementation of classical library management system.

## Core features

The system must be able to do:

- adding users (casual reader, administrator)
- adding rentable position
- getting positions details (with support of paging)
- removing position from inventory
- renting position(s) to given user
- returning position(s) to inventory

### Requirements

- `Gradle 7.4.2`
- `JDK 17`

### Build

#### Linux

```sh
cd LibraryManagementServer
gradle build
```
