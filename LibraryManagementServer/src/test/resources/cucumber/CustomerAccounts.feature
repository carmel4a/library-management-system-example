Feature: Customers accounts
  After that Customer is free to use our system.
  Customer may see his history of rented positions including currently rented positions.
  Customer may want to remove his account.

  Scenario: Customer want to create account
    Customers (book readers) must have account to rent some book from our Library.
    Customer must create online account on our website, then this acount is activated by one of our Administrators.

    Given new costumer has email JohnDoe@gmail.com
    And new customer uses password to "JohnDoe"
    When new customer want to create new account
    Then new account is created
    And customer may log in on his account
    But customer may not rent any position

  Rule: One account per email address is permited

    Background:
      Given customer with email JohnDoe@gmail.com exists in our system

    Example: new customer want to register with email JohnDoe@gmail.com
      Given new costumer has email JohnDoe@gmail.com
      When new customer want to create new account
      Then message describing the problem is shown
      And new account is not created
      And customer may not log in on his account

  Rule: Only activated customer accounts may rent book

    Background:
      Given customer with email JohnDoe@gmail.com exists in our system
      And customer with email JohnDoe@gmail.com is not activated

    Example: not activated customer want to rent a book
      When customer want to rent a book
      Then message describing the problem is shown

    Example: activated customer want to rent a book
      Given customer is activated by administrator
      When customer want to rent a book
      Then book is rented for given user
