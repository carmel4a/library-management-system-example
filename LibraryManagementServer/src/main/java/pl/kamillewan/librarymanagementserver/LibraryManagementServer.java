package pl.kamillewan.librarymanagementserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryManagementServer {

	public static void main(String[] args) {
		SpringApplication.run(LibraryManagementServer.class, args);
	}

}
